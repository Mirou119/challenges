package applicationservice;

import api.dto.ProductAssembler;
import api.dto.ProductDto;
import domain.product.Product;
import domain.product.ProductRepository;

public class ShoppingService {

    private ProductRepository productRepository;
    private ProductAssembler productAssembler;

    public ShoppingService(ProductAssembler productAssembler, ProductRepository productRepository) {
        this.productAssembler = productAssembler;
        this.productRepository = productRepository;
    }

    public ProductDto addProduct(ProductDto productDto) {
        Product product = productAssembler.create(productDto);
        productRepository.save(product);
        return productAssembler.create(product);
    }
}
