import api.ShoppingResource;
import api.dto.ProductAssembler;
import applicationservice.ShoppingService;
import domain.product.ProductRepository;
import http.CORSFilter;
import infrastructure.ProductRepositoryInMemory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

public class ShoppingServer implements Runnable {
    private static final int PORT = 8080;

    public static void main(String[] args) {
        new ShoppingServer().run();
    }

    private static HashSet<Object> getContextResources() {
        HashSet<Object> resources = new HashSet<>();

        ProductRepository productRepository = new ProductRepositoryInMemory();
        ProductAssembler productAssembler = new ProductAssembler();
        ShoppingService shoppingService = new ShoppingService(productAssembler, productRepository);
        ShoppingResource shoppingResource = new ShoppingResource(shoppingService);


        resources.add(shoppingResource);
        resources.add(shoppingService);
        return resources;
    }

    @Override
    public void run() {
        Server server = new Server(PORT);
        ServletContextHandler servletContextHandler = new ServletContextHandler(server, "/");

        ResourceConfig resourceConfig = ResourceConfig.forApplication(new Application() {
            @Override
            public Set<Object> getSingletons() {
                return getContextResources();
            }
        });

        resourceConfig.register(CORSFilter.class);
        ServletContainer container = new ServletContainer(resourceConfig);
        ServletHolder servletHolder = new ServletHolder(container);

        servletContextHandler.addServlet(servletHolder, "/*");

        try {
            server.start();
            server.join();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            server.destroy();
        }
    }
}
