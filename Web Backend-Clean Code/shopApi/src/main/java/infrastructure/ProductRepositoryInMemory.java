package infrastructure;

import domain.product.Product;
import domain.product.ProductAlreadyExistException;
import domain.product.ProductRepository;

import java.util.HashMap;
import java.util.Map;

public class ProductRepositoryInMemory implements ProductRepository {

    private final Map<String, Product> products = new HashMap<>();

    @Override
    public Product findByName(String productName) {

        return products.get(productName);
    }

    @Override
    public void save(Product product) {
        if (findByName(product.getName()) != null) {
            throw new ProductAlreadyExistException(
                    String.format("This product already exist.", product)
            );
        }
        products.put(product.getName(), product);
    }
}
