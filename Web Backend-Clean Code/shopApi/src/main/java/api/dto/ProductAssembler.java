package api.dto;

import domain.product.Product;

public class ProductAssembler {
    public Product create(ProductDto productDto) {
        Product product = new Product();
        product.setName(productDto.getName());
        product.setPrix(productDto.getPrix());
        product.setQuantity(productDto.getQuantity());
        return product;
    }

    public ProductDto create(Product product) {
        ProductDto productDto = new ProductDto();
        productDto.setId(product.getId());
        productDto.setName(product.getName());
        productDto.setPrix(product.getPrix());
        productDto.setQuantity(product.getQuantity());
        return productDto;
    }
}
