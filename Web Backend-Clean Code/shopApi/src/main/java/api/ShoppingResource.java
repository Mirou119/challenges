package api;

import api.dto.ProductDto;
import applicationservice.ShoppingService;
import domain.product.ProductAlreadyExistException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

@Path("/add")
public class ShoppingResource {

    private ShoppingService shoppingService;

    public ShoppingResource(ShoppingService shoppingService) {
        this.shoppingService = shoppingService;
    }

    @POST
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createProduct(ProductDto productDto) {
        try {
            productDto = shoppingService.addProduct(productDto);
            return Response.created(new URI(productDto.getId())).entity(productDto).build();
        } catch (ProductAlreadyExistException productNotFoundException) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } catch (URISyntaxException uriSyntaxException) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
