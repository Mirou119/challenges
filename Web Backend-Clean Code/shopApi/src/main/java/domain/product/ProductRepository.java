package domain.product;

public interface ProductRepository {
    Product findByName(String productId);

    void save(Product product);
}
