package repository;

import domain.product.Product;
import domain.product.ProductAlreadyExistException;
import domain.product.ProductRepository;
import infrastructure.ProductRepositoryInMemory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ShoppingRepositoryTest {
    @Mock
    private Product product;

    private ProductRepository productRepository;

    @Before
    public void setUp(){
        productRepository = new ProductRepositoryInMemory();
    }

    @Test(expected = ProductAlreadyExistException.class)
    public void givenAlreadyExistProduct_whenSave_thenTrowException(){
        productRepository.save(product);
        productRepository.save(product);
    }
}
