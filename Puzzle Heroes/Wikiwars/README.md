# Puzzle Heroes

## Wikiwars
Dans ce défi, vous devez trouver un chemin entre deux pages. Il faut ensuite que vous donniez la liste des pages parcourues. Le défi est considéré comme réussi, si un chemin est donné et valide pour chaque pages demandées.

### Chemins demandés

Computer science → Willy Wonka & the Chocolate Factory

https://en.wikipedia.org/wiki/Computer_science
https://en.wikipedia.org/wiki/Computer_graphics_(computer_science)
https://en.wikipedia.org/wiki/Utah_teapot
https://en.wikipedia.org/wiki/Toy_Story
https://en.wikipedia.org/wiki/Pixar
https://en.wikipedia.org/wiki/Film_studio
https://en.wikipedia.org/wiki/Production_company
https://en.wikipedia.org/wiki/Paramount_Pictures

fu that 

https://en.wikipedia.org/wiki/Willy_Wonka_%26_the_Chocolate_Factory

Matt Groening  → Brigitte Bardot

Tim Berners-Lee → Bob Ross

James Gosling → Damn Daniel

Széchenyi thermal bath → Joé Juneau

Time travel → Satoshi Nakamoto


Vous devez donner le nom des pages parcourues sous chacun des chemins.