# Puzzle Heroes

## Face Mash

Pour chaque photo dans ce répertoire, vous devez trouver quels sont les personnages de la culture populaire qui ont été mélangés pour donner ces magnifiques portraits.
* Il y a toujours DEUX personnages par photo.
* Pour avoir le point vous pouvez inscrire jusqu'à trois noms de personnages pour essayer de tomber sur les deux bons. Vous avez donc droit à une erreur par photo.

Écrire les réponses dans ce README!

#### Réponses:

###### Photo 1
![](photo 1.png)

votre réponse:
Anakin skywalker
Daryl dans walking dead

###### Photo 2
![](photo 2.png)

votre réponse:
Sasuke
Med mouine

###### Photo 3
![](photo 3.png)

votre réponse:
Un bonhomme mauve
la mère de Dan

###### Photo
![](photo 4.png)

votre réponse:
Flinders dans les SImpsons
Puppy bootle

###### Photo 5
![](photo 5.png)

votre réponse:
Julien Morisette
Kal drogo

###### Photo 6
![](photo 6.png)

votre réponse:
le ballon dans wilson
Tom Hanks

###### Photo 7
![](photo 7.png)


votre réponse:
Ginette Reneaud
Un morceau de robot 

###### Photo 8
![](photo 8.png)

votre réponse:
Fox,
Le pti gars dans Stranger things

###### Photo 9


![](photo 9.png)

votre réponse:
Abraham Lincoln

###### Photo 10
![](photo 10.png)

votre réponse:
Dr house
Nicolas Cage

